import yaml
import os

import gitlab


def lint_ci_file(filename):
    with open(filename, 'r') as f:
        content = f.read()

    gl = gitlab.Gitlab(os.environ['CI_SERVER_URL'], private_token=os.environ['CI_JOB_TOKEN'])
    project_id = os.environ['CI_PROJECT_ID']
    gl.projects.get(project_id).ci_lint.validate({"content": content})


if __name__ == '__main__':

    with open('./templates/base-pipeline.yml', 'r') as f:
        base_pipeline = yaml.safe_load(f)

    dockerfiles = [f for f in os.listdir('.') if os.path.isfile(f) and f.endswith('Dockerfile')]

    for dockerfile in dockerfiles:
        prefix = dockerfile.split('.')[0]
        with open('./templates/build-container-job.yml', 'r') as f:
            build_container = yaml.safe_load(f)

        job_name = list(build_container.keys())[0]
        build_job = build_container[job_name]
        build_job['variables']['IMAGE_NAME'] = f"/{prefix}"

        new_job_name = f"{job_name}-{prefix}"

        base_pipeline[new_job_name] = build_job

    print(yaml.dump(base_pipeline))
    with open('pipeline.yml', 'w') as f:
        yaml.dump(base_pipeline, f)

    lint_ci_file('pipeline.yml')
